@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab col s6"><a class="active" href="#json1">JSON manual</a></li>
                <li class="tab col s6"><a href="#apis">Consumiendo API</a></li>
            </ul>
        </div>
        <div id="json1" class="col s12">
            <br>
            <div class="containerP">
                <div class="row">
                    <div class="col s12">

                        <i class="material-icons prefix">assignment_ind</i>
                        <div class="input-field inline">
                            <input id="txtNombre" max="10" type="text" class="validate">
                            <label for="txtNombre">Nombre</label>
                        </div>

                        <a id="btnGuardar" class="waves-effect waves-light btn-small btn-primary">Guardar</a>
                        <a id="btnLimpiar" class="waves-effect waves-light btn btn-small btn-danger">Limpiar listado</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col s12">
                    <h6 class="center">Listado de personas</h6>
                </div>
            </div>

            <div class="containerP">
                <table id="listPersonas" class="highlight responsive-table">
                    <thead>
                    <tr>
                        <th>Nombre de la persona</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>

                    @foreach (json_decode($personajesArray) as $persona)

                        <tr>
                            <td>{{ $persona }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
        </div>
        <div id="apis" class="col s12">
            <br>
            <div class="row">
                <div class="col s12">
                    <span class="right">
                        <strong>API: </strong>
                        https://reqres.in/api/users
                    </span>
                </div>
                <div class="col s12">
                    <table id="listApi" class="highlight responsive-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Email</th>
                            <th>Nombres</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($personasJSON as $value)
                            <tr>
                                <td>{{$value["id"]}}</td>
                                <td>{{$value["email"]}}</td>
                                <td>{{$value["first_name"] . ' ' . $value["first_name"]}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection