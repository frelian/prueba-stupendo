<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Stupento | Prueba técnica</title>

    <!-- Estilos -->
    <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet" media="screen,projection">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>

    @include('header')

    <main>
        @yield('content')
        <a
            class="btn-floating btn-flotante btn-large waves-effect waves-light green right"
            href="https://wa.me/573138317429?text=Hola,%20queremos%20contactarte,%20tenemos%20buenas%20noticias..."
            target="_blank"

            >
            <i class="material-icons">perm_phone_msg</i>
        </a>
    </main>

@include('footer')
