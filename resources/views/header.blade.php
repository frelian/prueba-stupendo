<header>
    <div class="containePr">
        <a href="#" data-target="nav-mobile" class="top-nav sidenav-trigger full hide-on-large-only">
            <i class="material-icons">menu</i>
        </a>
    </div>
    <ul id="nav-mobile" class="sidenav sidenav-fixed" style="transform: translateX(0px);">


        <li class="bold active">
            <a href="/" class="waves-effect waves-green">Inicio</a>
        </li>

        <li>
            <div>
                <ul class="collection">
                    <li class="collection-item avatar">
                        <i class="material-icons circle blue">person</i>
                        <span class="title">Prueba técnica</span>
                        <p>
                            <strong>Para: </strong> Stupendo
                            <br>
                            <div class="right">
                                <strong>Hecha por:</strong> Julian Niño
                                <br>
                                <strong>Email: </strong> frelian@gmail.com
                                <strong>Celular: </strong> 313 831 7429
                            </div>
                        <br>

                        </p>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</header>