
<footer class="page-footer docs-footer">
    <div class="container">
        <div class="row" style="margin-bottom: 0;">
            <div class="col s12 m10 offset-m1">

                <div class="footer-copyright">
                    © Febrero <noscript>2020</noscript><script type="text/javascript">document.write(new Date().getFullYear());</script> - Julian Niño
                    <span>stupendo</span>
                </div>

            </div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script   src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="{{ asset('js/materialize.min.js') }}"></script>
<script src="{{ asset('js/init.js') }}"></script>

</body>
</html>