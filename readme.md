<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Sobre esta Web

- Prueba técnica para ingreso a Stupendo (Bogotá)
- Usada la versión LTS 5.5 de Laravel
- Frontend: Materializecss
- Se adantaron los colores (CSS) para adaptarlos a una gama azul
- Guardar el nombre de una persona y guardarlo temporalmente en un listado o tabla
- Se instalo el paquete guzzlehttp/guzzle (v6.5.2) para obtener datos de una api externa: https://reqres.in/api/users
- Uso de tabs, toast(mensajes), sidenav (menu)
- Detección al presionar tecla "enter" en el txtNombre se produce el evento
- Para el txtNombre, se valido que no estuviese vacio y su longitud