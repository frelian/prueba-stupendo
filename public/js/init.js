$( document ).ready(function(){

    // Inicializo elementos material
    $('.sidenav').sidenav(); // menu
    $('.collapsible').collapsible(); // por si llego a usar el submenu...
    $('.tabs').tabs(); // para las pestañas

    $('#txtNombre').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);

        if(keycode == '13'){
            if($("#txtNombre").val()){

                if($("#txtNombre").val().length <= 35){
                    guardarData();
                }
                else {
                    M.toast({html: 'Error, el nombre digitado supera los 35 carácteres', classes: 'rounded'});
                }
            }
        }

        event.stopPropagation();
    });


    // Capturo el evento "clic" para guardar en la tabla
    $( "#btnGuardar" ).click(function() {

        // 1ro valido si se envio algo en el input text: txtNombre
        if($("#txtNombre").val()){

            if($("#txtNombre").val().length <= 35){
                guardarData();
            }
            else {
                M.toast({html: 'Error, el nombre digitado supera los 35 carácteres', classes: 'rounded'});
            }
        }
    });

    // Boton para limpiar el listado de usuarios de la tabla
    $( "#btnLimpiar" ).click(function() {

        $('#listPersonas tbody').empty();
        $("#txtNombre").val("");
    });

    // Funcion para almacenar datos en la tabla
    function guardarData() {

        M.toast({html: 'Nombre agregado...', classes: 'rounded'});

        var txtNom = $("#txtNombre").val();

        // listPersonas
        $("#listPersonas").last().append("<tr><td>" + txtNom + "</td></tr>");

        // luego de agregar el nombre vacio el input de nombre
        $("#txtNombre").val("");
    }
});