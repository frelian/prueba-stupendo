<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class PersonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Personajes publicos: mediante un array llenado manualmente
        $miArray = array("Linus Torvalds", "Richard Stallman", "Ian Murdock", "Matthias Ettrich");
        $personajesArray = json_encode($miArray);

        // Obtengo datos de la API publica
        $client = new Client();
        $res = $client->request('GET', 'https://reqres.in/api/users');
        $result = $res->getBody();
        $clientes = json_decode($result, true);

        return view('personas')
            ->with('personajesArray', $personajesArray)
            ->with('personasJSON', $clientes['data']);
    }
}
